import binascii
from posix.types cimport gid_t, pid_t, off_t, uid_t
from posix.stat cimport struct_stat, fstat
from struct import unpack

cdef extern from "sys/mman.h":
    void * mmap(void *, size_t, int, int, int, off_t) nogil
    int shm_open(char *, int, mode_t) nogil
    cdef enum:
        PROT_READ "PROT_READ"
        MAP_SHARED "MAP_SHARED"
        MAP_FAILED "MAP_FAILED"

cdef extern from "sys/fcntl.h":
    int open(char *fileName, int access, int permission) nogil
    cdef enum:
        O_RDONLY "O_RDONLY"
        S_IREAD "S_IREAD"

cdef extern from "unistd.h":
    int close(int) nogil


cdef class py_mmap_reader_t:
    cdef unsigned char *g_raw_bytes
    cdef int curr_offset
    cdef unsigned long size
    cdef object fileName
    cdef object unsigned_int_formats_single_elem
    cdef object unsigned_int_formats_multi_elem
    cdef object signed_int_formats_single_elem
    cdef object signed_int_formats_multi_elem
    cdef object float_formats_single_elem
    cdef object float_formats_multi_elem
    cdef unsigned short is_little_endian
    def __init__(self, bin_file_name, raw_pointer = 0x00):

        self.unsigned_int_formats_single_elem = list([""]*10)
        self.unsigned_int_formats_single_elem[1] = "<B"
        self.unsigned_int_formats_single_elem[2] = "<H"
        self.unsigned_int_formats_single_elem[4] = "<I"
        self.unsigned_int_formats_single_elem[8] = "<Q"
        self.unsigned_int_formats_multi_elem = list([""]*10)
        self.unsigned_int_formats_multi_elem[1] = "<{0}B"
        self.unsigned_int_formats_multi_elem[2] = "<{0}H"
        self.unsigned_int_formats_multi_elem[4] = "<{0}I"
        self.unsigned_int_formats_multi_elem[8] = "<{0}Q"

        self.signed_int_formats_single_elem = list([""]*10)
        self.signed_int_formats_single_elem[1] = "<b"
        self.signed_int_formats_single_elem[2] = "<h"
        self.signed_int_formats_single_elem[4] = "<i"
        self.signed_int_formats_single_elem[8] = "<q"
        self.signed_int_formats_multi_elem = list([""]*10)
        self.signed_int_formats_multi_elem[1] = "<{0}b"
        self.signed_int_formats_multi_elem[2] = "<{0}h"
        self.signed_int_formats_multi_elem[4] = "<{0}i"
        self.signed_int_formats_multi_elem[8] = "<{0}q"
        
        self.float_formats_single_elem = list([""]*10)
        self.float_formats_single_elem[4] = "<f"
        self.float_formats_single_elem[8] = "<d"
        self.float_formats_multi_elem = list([""]*10)
        self.float_formats_multi_elem[4] = "<{0}f"
        self.float_formats_multi_elem[8] = "<{0}d"

        self.is_little_endian = True
        self.fileName = bin_file_name
        self.size = self.get_file_size()
        if self.size:
            self.curr_offset = 0
            if raw_pointer:
                pass
            elif len(bin_file_name):
                f = open(bin_file_name.encode(), O_RDONLY, S_IREAD)
                self.g_raw_bytes = <unsigned char *> mmap(NULL, self.size, PROT_READ, MAP_SHARED, f, 0)

    def set_current_offset(self, offset):
        self.curr_offset = offset

    def increment_current_offset(self, inc):
        self.curr_offset += inc

    def get_current_offset(self):
        return self.curr_offset

    def set_little_endian(self):
        self.is_little_endian = True

    def set_big_endian(self):
        self.is_little_endian = False

    def get_file_size(self):
        cdef struct_stat stat;
        f = open(self.fileName.encode(), O_RDONLY, S_IREAD)
        fstat(f, &stat)
        close(f)
        return stat.st_size

    def get_bytes(self, num_bytes):
        if self.g_raw_bytes and (self.curr_offset+num_bytes)<self.size:
            self.curr_offset += num_bytes
            ret_bytes = self.g_raw_bytes[self.curr_offset-num_bytes:self.curr_offset]
            #print("{0} @ 0x{1:08x} : {2}".format(num_bytes, self.curr_offset-num_bytes,
            #    binascii.hexlify(ret_bytes)))
            return ret_bytes
        else:
            raise ValueError("All bytes over")

    def get_unsigned_int(self, num_bytes, num_elem=1):
        if (num_elem==1):
            decode_format = self.unsigned_int_formats_single_elem[num_bytes]
        else:
            decode_format = self.unsigned_int_formats_multi_elem[num_bytes].format(num_elem)
        return unpack(decode_format, self.get_bytes(num_bytes*num_elem))

    def get_signed_int(self, num_bytes, num_elem=1):
        if (num_elem==1):
            decode_format = self.signed_int_formats_single_elem[num_bytes]
        else:
            decode_format = self.signed_int_formats_multi_elem[num_bytes].format(num_elem)
        return unpack(decode_format, self.get_bytes(num_bytes*num_elem))

    def get_char(self, num_bytes):
        return self.get_bytes(num_bytes)

    def get_float(self, num_bytes, num_elem=1):
        if (num_elem==1):
            decode_format = self.float_formats_single_elem[num_bytes]
        else:
            decode_format = self.float_formats_multi_elem[num_bytes].format(num_elem)
        return unpack(decode_format, self.get_bytes(num_bytes))

    def get_pointer(self, num_bytes, num_elem=1):
        return self.get_unsigned_int(num_bytes)




