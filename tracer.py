import json
import pickle
import argparse
import py_mmap_reader
import binascii
from pdb import set_trace
from py_cstruct_parser import *
from collections import namedtuple

class tracer_t:
    mapping_info_t = namedtuple("mapping_info_t", ["datatype", "crc", "crc_fields"])
    def __init__(self, source_file, trace_file):
        self.source_file = source_file
        self.trace_file  = trace_file
        self.reader = py_mmap_reader.py_mmap_reader_t(trace_file)
        #self.parser = pickle.load(open("./cdefs.pickle", "rb"))
        #self.parser.mmap_reader = self.reader
        self.parser = py_cstruct_parser_t(source_file, self.reader)
        self.id_to_datatype = {};
 
    def get_field_types_for_crc(self, datatype):
        field_type_names = list()
        if datatype.enum==data_types_enum_t.struct:
            for member_name, member in datatype.members_list.items():
                if member.subtype.enum==data_types_enum_t.basic:
                    field_type_names.append(member.subtype.name)
                elif member.subtype.enum==data_types_enum_t.array:
                    if member.subtype.name:
                        field_type_names.append(member.subtype.name)
                    else:
                        if member.subtype.subtype.enum==data_types_enum_t.typedef:
                            field_type_names.extend(self.get_field_types_for_crc(member.subtype.subtype))
                elif member.subtype.enum==data_types_enum_t.typedef:
                    if member.subtype.name:
                        field_type_names.append(member.subtype.name)
                elif member.subtype.enum==data_types_enum_t.struct:
                    if member.subtype.name:
                        field_type_names.extend(self.get_field_types_for_crc(member.subtype))
                elif member.subtype.enum==data_types_enum_t.union:
                    if member.subtype.name:
                        field_type_names.extend(self.get_field_types_for_crc(member.subtype))
                elif member.subtype.enum==data_types_enum_t.pointer:
                    field_type_names.append(member.subtype.name)
                else:
                    set_trace()
                    pass
        elif datatype.enum==data_types_enum_t.basic:
            field_type_names.append(datatype.name)
        elif datatype.enum==data_types_enum_t.union:
            pass
        elif datatype.enum==data_types_enum_t.array:
            field_type_names.extend(self.get_field_types_for_crc(datatype.subtype))
        elif datatype.enum==data_types_enum_t.enum:
            if datatype.name:
                field_type_names.append(datatype.name)
        elif datatype.enum==data_types_enum_t.pointer:
            if datatype.name:
                field_type_names.append(datatype.name)
        elif datatype.enum==data_types_enum_t.typedef:
            if datatype.name:
                field_type_names.append(datatype.name)
            else:
                field_type_names.extend(self.get_field_types_for_crc(datatype.subtype))
        else:
            set_trace()
            pass
        return field_type_names

               

    def map_struct_to_id(self):
        names1 = [];
        names2 = [];
        for typename in self.parser.definitions.keys():
            datatype = self.parser.get_final_datatype(typename)
            crc = None
            type_names = [typename]
            type_names.extend(self.get_field_types_for_crc(datatype))
            crc = hex(binascii.crc32(" ".join(type_names).encode()))
            #print("{0} : {1} : {2} : {3}".format(typename, datatype.size, crc, type_names))
            mapping_info = self.mapping_info_t(datatype=datatype, crc=crc, crc_fields=type_names)
            self.id_to_datatype[crc] = mapping_info

    def map_id_to_struct(self):
        for key in self.parser.instances.keys():
            inst = self.parser.instances[key]

    def trace_decode(self):
        preamble_datatype = self.parser.definitions['l1df_trace_preamble']
        #print("Decoding as type {0}, of size {1}".format(preamble_datatype.name, preamble_datatype.size))
        trace_preamble = self.parser.decode_as_type(preamble_datatype)
        print(json.dumps(trace_preamble))
        arch = arch_types_enum_t.M32;
        if trace_preamble['endianness']==int("0x01020304", 0):
            # Target is little endian machine
            endian = endian_types_enum_t.LittleEndian
        else :
            endian = endian_types_enum_t.BigEndian

        # Read through and decode 
        trace_hdr_datatype = self.parser.get_final_datatype('l1df_trace_hdr')
        while (True):
            #print("Decoding as type {0}, of size {1} from offset {2}".format(trace_hdr_datatype.name, trace_hdr_datatype.size,
            #    hex(self.parser.mmap_reader.get_current_offset())))
            try:
                trace_hdr = self.parser.decode_as_type(trace_hdr_datatype)
            except:
                exit()
            print(json.dumps(trace_hdr, indent=4))
            if trace_hdr['id']==0:
                self.parser.mmap_reader.increment_current_offset(trace_hdr['size'])
                continue
            [struct_datatype, _, _] = self.id_to_datatype[hex(trace_hdr['id'])]
            count  = int(trace_hdr['size']/struct_datatype.size)
            current_offset = self.parser.mmap_reader.get_current_offset()
            size = trace_hdr['size']
            if count==1:
                trace_struct = self.parser.decode_as_type(struct_datatype)
                print(json.dumps(trace_struct, indent=4))
            else:
                for i in range(count):
                    trace_struct = self.parser.decode_as_type(struct_datatype)
                    print(json.dumps(trace_struct, indent=4))
            self.parser.mmap_reader.set_current_offset(current_offset + size)


if __name__ == "__main__":
    parser = argparse.ArgumentParser();
    parser.add_argument('fnames', nargs='+');
    parse_res = parser.parse_args();
    fnames = parse_res.fnames;

    tracer = tracer_t(fnames[0], fnames[1])
    tracer.map_struct_to_id()
    tracer.map_id_to_struct()
    tracer.trace_decode()


