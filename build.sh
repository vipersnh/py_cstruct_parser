set -x;
cython py_mmap_reader.pyx
gcc -shared -o py_mmap_reader.so -fPIC py_mmap_reader.c -I/usr/include/python3.3
rm py_mmap_reader.c
set +x;
