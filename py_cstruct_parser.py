from copy import deepcopy
from pdb import set_trace
from pycparser import parse_file, c_parser, c_generator
from py_mmap_reader import py_mmap_reader_t
from collections import namedtuple, OrderedDict
from pycparser.c_ast import (Typedef, TypeDecl, Typename, Struct, Enum, Constant, IdentifierType, 
        ID, NamedInitializer, Decl, UnaryOp, Union, IdentifierType, Enum, PtrDecl, ArrayDecl)


class arch_types_enum_t:
    M32 = 0
    M64 = 1

class signedness_t:
    UNDEFINED = -1
    UNSIGNED = 0
    SIGNED = 1

class endian_types_enum_t:
    LittleEndian=0
    BigEndian=1

class data_types_enum_t:
    enum = 0
    basic = 1
    array = 2
    struct = 3
    union = 4
    pointer = 5
    typedef = 6
    instance = 7

class data_type_enum_t:
    def __init__(self, enum, name, initialized, enums, size, alignment):
        self.enum = enum
        self.name = name
        self.initialized = initialized
        self.enums = enums
        self.size = size
        self.alignment = alignment

class data_type_basic_t:
    def __init__(self, enum, name, initialized, is_signed, size, alignment):
        self.enum = enum
        self.name = name
        self.initialized = initialized
        self.is_signed = is_signed
        self.size = size
        self.alignment = alignment

class data_type_member_t:
    def __init__(self, name, initialized, subtype, byte_offset, size, alignment):
        self.name = name
        self.initialized = initialized
        self.subtype = subtype
        self.byte_offset = byte_offset
        self.size = size            # Redundent information, added for speed improvement
        self.alignment = alignment  # Redundent information, added for speed improvement

class data_type_struct_t:
    def __init__(self, enum, name, initialized, members_list, size, alignment):
        self.enum = enum
        self.name = name
        self.initialized = initialized
        self.members_list = members_list
        self.size = size
        self.alignment = alignment

class data_type_union_t:
    def __init__(self, enum, name, initialized, members_list, size, alignment):
        self.enum = enum
        self.name = name
        self.initialized = initialized
        self.members_list = members_list
        self.size = size
        self.alignment = alignment

class data_type_array_t:
    def __init__(self, enum, name, initialized, dims, subtype, size, alignment):
        self.enum = enum
        self.name = name
        self.initialized = initialized
        self.dims = dims
        self.subtype = subtype
        self.size = size
        self.alignment = alignment

class data_type_pointer_t:
    def __init__(self, enum, name, initialized, subtype, size, alignment):
        self.enum = enum
        self.name = name
        self.initialized = initialized
        self.subtype = subtype
        self.size = size
        self.alignment = alignment

class data_type_typedef_t:
    def __init__(self, enum, name, initialized, subtype, size, alignment):
        self.enum = enum
        self.name = name
        self.initialized = initialized
        self.subtype = subtype
        self.size = size
        self.alignment = alignment

class data_instance_t:
    def __init__(self, enum, name, initialized, subtype, members):
        self.enum = enum
        self.name = name
        self.initialized = initialized
        self.subtype = subtype
        self.members = members

class py_cstruct_parser_t:
    def __init__(self, c_file, mmap_reader, arch=arch_types_enum_t.M32,
            endian=endian_types_enum_t.LittleEndian):
        self.mmap_reader = mmap_reader
        self.enum_auto_mode = True
        self.definitions = dict()
        self.instances = dict()
        self.target_arch = arch
        self.target_endian = endian
        self.target_word_size = 4 if arch==arch_types_enum_t.M32 else 8
        self.target_max_align = self.target_word_size
        self.update_basic_definitions()
        self.update_definitions(parse_file(c_file, use_cpp=True))
        self.update_definitions_recurse()

    def print_all_typenames(self):
        for key, value in self.definitions.items():
            print(key)

    def update_basic_definitions(self):
        enum = data_types_enum_t.basic
        typename = "void"
        self.definitions[typename] = data_type_basic_t(
                enum = enum,
                name = typename,
                initialized = True,
                is_signed = signedness_t.UNDEFINED,
                size = 0,
                alignment = 0,
                )

        typename = "__ptr__"
        self.definitions[typename] = data_type_basic_t(
                enum = enum,
                name = typename,
                initialized = True,
                is_signed = signedness_t.UNDEFINED,
                size = 8 if self.target_arch==arch_types_enum_t.M64 else 4,
                alignment = 8 if self.target_arch==arch_types_enum_t.M64 else 4,
                )

        typename = "enum"
        self.definitions[typename] = data_type_basic_t(
                enum = enum,
                name = typename,
                initialized = True,
                is_signed = signedness_t.UNSIGNED,
                size = 8 if self.target_arch==arch_types_enum_t.M64 else 4,
                alignment = 8 if self.target_arch==arch_types_enum_t.M64 else 4,
                )

        typename = "char"
        self.definitions[typename] = data_type_basic_t(
                enum = enum,
                name = typename,
                initialized = True,
                is_signed = signedness_t.SIGNED,
                size = 1,
                alignment = 1,
                )

        typename = "signed char"
        self.definitions[typename] = data_type_basic_t(
                enum = enum,
                name = typename,
                initialized = True,
                is_signed = signedness_t.SIGNED,
                size = 1,
                alignment = 1,
                )

        typename = "unsigned char"
        self.definitions[typename] = data_type_basic_t(
                enum = enum,
                name = typename,
                initialized = True,
                is_signed = signedness_t.UNSIGNED,
                size = 1,
                alignment = 1,
                )

        typename = "short"
        self.definitions[typename] = data_type_basic_t(
                enum = enum,
                name = typename,
                initialized = True,
                is_signed = signedness_t.SIGNED,
                size = 2,
                alignment = 2,
                )

        typename = "signed short"
        self.definitions[typename] = deepcopy(self.definitions["short"])
        self.definitions[typename].name = typename

        typename = "unsigned short"
        self.definitions[typename] = data_type_basic_t(
                enum = enum,
                name = typename,
                initialized = True,
                is_signed = signedness_t.UNSIGNED,
                size = 2,
                alignment = 2,
                )

        typename = "short int"
        self.definitions[typename] = deepcopy(self.definitions["short"])
        self.definitions[typename].name = typename

        typename = "signed short int"
        self.definitions[typename] = deepcopy(self.definitions["short"])
        self.definitions[typename].name = typename

        typename = "unsigned short int"
        self.definitions[typename] = deepcopy(self.definitions["unsigned short"])
        self.definitions[typename].name = typename

        typename = "int"
        self.definitions[typename] = data_type_basic_t(
                enum = enum,
                name = typename,
                initialized = True,
                is_signed = signedness_t.UNSIGNED,
                size = 4,
                alignment = 4,
                )

        typename = "signed int"
        self.definitions[typename] = deepcopy(self.definitions["int"])
        self.definitions[typename].name = typename

        typename = "unsigned int"
        self.definitions[typename] = data_type_basic_t(
                enum = enum,
                name = typename,
                initialized = True,
                is_signed = signedness_t.UNSIGNED,
                size = 4,
                alignment = 4,
                )

        typename = "long"
        self.definitions[typename] = data_type_basic_t(
                enum = enum,
                name = typename,
                initialized = True,
                is_signed = signedness_t.SIGNED,
                size = 8 if self.target_arch==arch_types_enum_t.M64 else 4,
                alignment = 8 if self.target_arch==arch_types_enum_t.M64 else 4,
                )

        typename = "signed long"
        self.definitions[typename] = deepcopy(self.definitions["long"])
        self.definitions[typename].name = typename

        typename = "unsigned long"
        self.definitions[typename] = data_type_basic_t(
                enum = enum,
                name = typename,
                initialized = True,
                is_signed = signedness_t.UNSIGNED,
                size = 8 if self.target_arch==arch_types_enum_t.M64 else 4,
                alignment = 8 if self.target_arch==arch_types_enum_t.M64 else 4,
                )

        typename = "long int"
        self.definitions[typename] = deepcopy(self.definitions["long"])
        self.definitions[typename].name = typename

        typename = "signed long int"
        self.definitions[typename] = deepcopy(self.definitions["long"])
        self.definitions[typename].name = typename

        typename = "unsigned long int"
        self.definitions[typename] = deepcopy(self.definitions["unsigned long"])
        self.definitions[typename].name = typename

        typename = "long long"
        self.definitions[typename] = data_type_basic_t(
                enum = enum,
                name = typename,
                initialized = True,
                is_signed = signedness_t.SIGNED,
                size = 8,
                alignment = 8,
                )
                
        typename = "signed long long"
        self.definitions[typename] = deepcopy(self.definitions["long long"])
        self.definitions[typename].name = typename

        typename = "unsigned long long"
        self.definitions[typename] = data_type_basic_t(
                enum = enum,
                name = typename,
                initialized = True,
                is_signed = signedness_t.UNSIGNED,
                size = 8,
                alignment = 8,
                )

        typename = "float"
        self.definitions[typename] = data_type_basic_t(
                enum = enum,
                name = typename,
                initialized = True,
                is_signed = signedness_t.SIGNED,
                size = 4,
                alignment = 4,
                )

        typename = "double"
        self.definitions[typename] = data_type_basic_t(
                enum = enum,
                name = typename,
                initialized = True,
                is_signed = signedness_t.SIGNED,
                size = 8,
                alignment = 4,
                )

        typename = "long double"
        self.definitions[typename] = data_type_basic_t(
                enum = enum,
                name = typename,
                initialized = True,
                is_signed = signedness_t.SIGNED,
                size = 12,
                alignment = 4,
                )

    def get_final_datatype(self, typename):
        datatype = self.update_definition_for_name(typename)
        if datatype.enum==data_types_enum_t.typedef:
            return datatype.subtype
        else:
            return datatype

    def update_definitions(self, ast):
        for i in ast.ext:
            if type(i)==Decl and i.name:
                [declname, typename, instance] = self.get_instance_from_ast(i)
                if declname:
                    self.instances[declname] = instance
            else:
                [declname, typename, datatype] = self.get_definition_from_ast(i)
                self.definitions[declname] = datatype

    def get_init_value(self, init):
        name = None
        if type(init)==NamedInitializer:
            [_, value] = self.get_init_value(init.expr)
            name = init.name[0].name
            return [name, value]
        elif type(init)==Constant:
            typename = init.type
            if self.definitions[typename].enum==data_types_enum_t.basic:
                value = init.value
                return [name, value]
            else:
                set_trace()
                pass
        elif type(init)==UnaryOp:
            value = self.evaluate_expr(init)
            return [name, value]

    def get_var_name(self, expr):
        return expr.expr.name

    def get_type_name(self, expr):
        if type(expr)==UnaryOp:
            return self.get_type_name(expr.expr)
        elif type(expr)==ID:
            return None;
        elif type(expr)==Typename:
            return self.get_type_name(expr.type)
        elif type(expr)==TypeDecl:
            return self.get_type_name(expr.type)
        elif type(expr)==IdentifierType:
            return " ".join(expr.names)
        else:
            set_trace()
            pass

    def evaluate_expr(self, expr):
        if expr.op=='sizeof':
            var_name = self.get_var_name(expr)
            typename = self.get_type_name(expr)
            if var_name in self.instances.keys():
                return self.instances[var_name].type.size
            elif typename in self.definitions.keys():
                self.update_definition_for_name(typename)
                size = self.definitions[typename].size
                return size
            else:
                set_trace()
                pass
        else:
            set_trace()
            pass

    def get_instance_from_ast(self, ast):
        declname = ast.name
        try:
            typename = ast.type.type.names[0]
        except:
            return [None, None, None]
        datatype = self.get_final_datatype(typename)
        if datatype.enum==data_types_enum_t.struct:
            if ast.init:
                sub_insts = OrderedDict()
                for idx, init in enumerate(ast.init.exprs):
                    [name, sub_inst] = self.get_init_value(init)
                    if name:
                        sub_insts[name] = sub_inst
                    else:
                        sub_insts[idx] = sub_inst
                values = []
                for idx, member_name in enumerate(datatype.members_list):
                    member = datatype.members_list[member_name]
                    member_type = member.subtype
                    if member.name in sub_insts.keys():
                        # Named initialization
                        sub_inst = sub_insts[member.name]
                        values.append(data_instance_t(
                            enum=data_types_enum_t.instance,
                            name = member.name,
                            initialized = True,
                            subtype = member_type,
                            members = sub_inst))
                    elif idx in sub_insts.keys():
                        # Indexed initialization
                        sub_inst = sub_insts[idx]
                        values.append(data_instance_t(
                            enum=data_types_enum_t.instance,
                            name = member.name,
                            initialized = True,
                            subtype = member_type,
                            members = sub_inst))
                    else:
                        set_trace()
                        values.append(inst_info_t(field.var_name, None, field.type, 
                            None))
                return [declname, datatype.name, data_instance_t(
                        enum = data_types_enum_t.instance,
                        name = declname,
                        initialized = True,
                        subtype = datatype,
                        members = values)]
        elif datatype.enum==data_types_enum_t.basic:
            if ast.init:
                init = ast.init.exprs[0]
                assert len(ast.init.exprs)==1, "What to do"
                [name, sub_init] = self.get_init_value(init)
            else:
                # Uninitialized declaration
                return inst_info_t(declname, qualifier, datatype, None)
        
        

    def get_definition_from_ast(self, ast):
        decl_type = type(ast)
        if decl_type==TypeDecl:
            declname = ast.declname
            [_, typename, datatype] = self.get_definition_from_ast(ast.type) 
            return [declname, typename, datatype]
        elif decl_type==Struct:
            declname = ast.name
            members_list = OrderedDict()
            if ast.decls:
                # Has actual definition
                for decl in ast.decls:
                    [member_name, member_type_name, member_type] = self.get_definition_from_ast(decl)
                    if member_type_name:
                        assert type(member_type_name)==str
                        member_subtype = self.definitions[member_type_name]
                    else:
                        member_subtype = member_type
                    member = data_type_member_t(
                            name = member_name,
                            initialized = False,
                            subtype = member_subtype,
                            byte_offset = None,
                            size = None,
                            alignment = None)
                    members_list[member_name] = member
            return [ declname, declname, data_type_struct_t(
                                enum = data_types_enum_t.struct,
                                name = None,
                                initialized = False,
                                members_list = members_list,
                                size = None,
                                alignment = None) ]

        elif decl_type==Union:
            declname = ast.name
            members_list = OrderedDict()
            if ast.decls:
                # Has actual definition
                for decl in ast.decls:
                    [member_name, member_type_name, member_type] = self.get_definition_from_ast(decl)
                    if member_type_name:
                        assert type(member_type_name)==str
                        member_subtype = self.definitions[member_type_name]
                    else:
                        member_subtype = member_type
                    member = data_type_member_t(
                            name = member_name,
                            initialized = False,
                            subtype = member_subtype,
                            byte_offset = None,
                            size = None,
                            alignment = None)
                    members_list[member_name] = member
            return [ declname, declname, data_type_union_t(
                                enum = data_types_enum_t.union,
                                name = None,
                                initialized = False,
                                members_list = members_list,
                                size = None,
                                alignment = None) ]
        elif decl_type==PtrDecl:
            [declname, typename, subtype] = self.get_definition_from_ast(ast.type)
            assert type(typename)==str
            pointer_type = self.definitions["__ptr__"]
            return [ declname, None, data_type_pointer_t(
                                    enum = data_types_enum_t.pointer,
                                    name = typename,
                                    initialized = False,
                                    subtype = pointer_type,
                                    size = self.calc_data_type_size(pointer_type),
                                    alignment = self.calc_data_type_alignment(pointer_type))]

        elif decl_type==ArrayDecl:
            [declname, typename, subtype] = self.get_definition_from_ast(ast.type)
            if typename:
                subtype = self.definitions[typename]
            else:
                subtype = subtype
            array_len = int(ast.dim.value)
            datatype = data_type_array_t(
                        enum = data_types_enum_t.array,
                        name = None,
                        initialized = False,
                        dims = array_len,
                        subtype = subtype,
                        size = None,
                        alignment = None)
            return [ declname, None, datatype]
        elif decl_type==IdentifierType:
            datatype_name = " ".join(ast.names)
            if datatype_name in self.definitions:
                return [datatype_name, datatype_name, self.definitions[datatype_name]]
            else:
                set_trace()
                pass
        elif decl_type==Typedef:
            [declname, typename, subtype] = self.get_definition_from_ast(ast.type)
            if typename:
                if typename in self.definitions.keys():
                    subtype = self.definitions[typename]
                else:
                    subtype.name = typename
                size = alignment = None
            else:
                size = subtype.size
                alignment = subtype.alignment
            return [ declname, typename,
                     data_type_typedef_t(
                        enum = data_types_enum_t.typedef,
                        name = declname,
                        initialized = False,
                        subtype = subtype,
                        size = size,
                        alignment = alignment )]
        elif decl_type==Enum:
            declname = ast.name
            subtype = self.definitions['enum']
            curr_value = 0
            enums = OrderedDict()
            for enum_value in ast.values.enumerators:
                enum_name = enum_value.name
                if enum_value.value:
                    try:
                        value = int(enum_value.value.value)
                    except:
                        pass
                    try:
                        value = int(enum_value.value.expr.value)
                        op = enum_value.value.op
                        if op=='-':
                            value = -value
                    except:
                        pass
                    curr_value = value
                enums[curr_value] = enum_name
                curr_value += 1
            return [declname, declname, data_type_enum_t(
                                enum = data_types_enum_t.enum,
                                name = None,
                                initialized = False,
                                enums = enums,
                                size = None,
                                alignment = None) ]

        elif decl_type==Decl:
            return self.get_definition_from_ast(ast.type)
        else:
            set_trace()
            pass
    
    def calc_enum_type_size_n_alignment(self, enum_type):
        assert enum_type.enum==data_types_enum_t.enum
        size = None
        if self.enum_auto_mode==False:
            size = self.definitions['enum'].size
        else:
            min_value = min(enum_type.enums.keys())
            max_value = max(enum_type.enums.keys())
            if min_value < 0:
                range_value = max_value - min_value
            else:
                range_value = max_value
            if range_value<pow(2,8*1-1):
                size = 1
            elif range_value<pow(2,8*2-1):
                size = 2
            elif range_value<pow(2,8*4-1):
                size = 4
            elif range_value<pow(2,8*8-1):
                size = 8
            else:
                set_trace()
                pass
        #print("{0} : {1}".format(size, enum_type.enums))
        enum_type.size = size
        enum_type.alignment = size
        enum_type.initialized = True
        return size

    def calc_data_type_size(self, datatype):
        if datatype.initialized:
            return datatype.size
        if datatype.enum==data_types_enum_t.basic:
            if datatype.size:
                return datatype.size
            else:
                set_trace()
                pass
        elif datatype.enum==data_types_enum_t.pointer:
            datatype.initialized = True
            datatype.size = self.target_word_size
            datatype.alignment = self.target_word_size
            return self.target_word_size;
        elif datatype.enum==data_types_enum_t.typedef:
            datatype = self.update_definition_for_typedef(datatype, None)
            assert datatype.initialized
            return datatype.size
        elif datatype.enum==data_types_enum_t.array:
            subtype = datatype.subtype
            if subtype.initialized==False:
                if subtype.enum==data_types_enum_t.struct:
                    subtype = self.update_definition_for_struct(subtype, None)
                    assert subtype.initialized
                elif subtype.enum==data_types_enum_t.union:
                    subtype = self.update_definition_for_union(subtype, None)
                    assert subtype.initialized
                elif subtype.enum==data_types_enum_t.pointer:
                    subtype = self.update_definition_for_pointer(subtype, None)
                    assert subtype.initialized
                elif subtype.enum==data_types_enum_t.array:
                    subtype = self.update_definition_for_array(subtype, None)
                    assert subtype.initialized
                elif subtype.enum==data_types_enum_t.typedef:
                    subtype = self.update_definition_for_typedef(subtype, None)
                    assert subtype.initialized
                else:
                    set_trace()
                    pass
            datatype.initialized = True
            datatype.size = datatype.dims * subtype.size
            datatype.alignment = subtype.alignment
            return datatype.size
        elif datatype.enum==data_types_enum_t.struct:
            datatype = self.update_definition_for_struct(datatype, None)
            assert datatype.initialized
            return datatype.size
        elif datatype.enum==data_types_enum_t.union:
            datatype = self.update_definition_for_union(datatype, None)
            assert datatype.initialized
            return datatype.size
        elif datatype.enum==data_types_enum_t.enum:
            return self.calc_enum_type_size_n_alignment(datatype)
        else:
            set_trace()
            pass

    def calc_data_type_alignment(self, datatype):
        if datatype.initialized:
            return datatype.alignment
        if datatype.enum==data_types_enum_t.array:
            subtype = datatype.subtype
            return subtype.alignment
        elif datatype.enum==data_types_enum_t.pointer:
            return self.target_word_size
        elif datatype.enum==data_types_enum_t.enum:
            return self.calc_enum_type_size_n_alignment(datatype)
        else:
            return datatype.alignment
    
    def update_definition_for_member(self, member, name):
        if member.initialized:
            return member
        if name:
            member.name = name
        datatype = member.subtype
        size = self.calc_data_type_size(datatype)
        alignment = self.calc_data_type_alignment(datatype)
        if size==0:
            set_trace()
            pass
        member.initialized = True
        member.size = size
        member.alignment = alignment
        return member

    def update_definition_for_struct(self, datatype, name):
        if datatype.initialized:
            return datatype
        if name:
            datatype.name = name
        if len(datatype.members_list)==0:
            # This case refers to the forward declared typedef struct
            datatype = self.definitions[datatype.name]
        byte_offset = 0
        alignment = 0
        for member_name, member in datatype.members_list.items():
            member = self.update_definition_for_member(member, None)
            member_size = member.size
            member_alignment = member.alignment
            if member_alignment>alignment:
                alignment = member_alignment
            while (byte_offset%member_alignment)!=0:
                byte_offset += 1
            member.byte_offset = byte_offset
            byte_offset += member_size
        if alignment==0:
            set_trace()
            pass
        while (byte_offset%alignment)!=0:
            byte_offset += 1
        datatype.initialized = True
        datatype.size = byte_offset
        datatype.alignment = alignment
        return datatype

    def update_definition_for_union(self, datatype, name):
        if datatype.initialized:
            return datatype
        if name:
            datatype.name = name
        max_size = 0
        alignment = 0
        for member_name, member in datatype.members_list.items():
            byte_offset = 0
            member = self.update_definition_for_member(member, None)
            member_size = member.size
            member_alignment = member.alignment
            if member_alignment>alignment:
                alignment = member_alignment
            while (byte_offset%member_alignment)!=0:
                byte_offset += 1
            member.byte_offset = byte_offset
            byte_offset += member_size
            if member_size > max_size:
                max_size = member_size
        while (byte_offset%alignment)!=0:
            byte_offset += 1
        datatype.initialized = True
        datatype.size = max_size
        datatype.alignment = alignment
        return datatype

    def update_definition_for_pointer(self, datatype, name):
        if datatype.initialized:
            return datatype
        if name:
            datatype.name = name
        referred_size = self.calc_data_type_size(datatype)
        referred_alignment = self.calc_data_type_alignment(datatype)
        datatype.initialized = True
        datatype.size = referred_size
        datatype.alignment = referred_alignment
        return datatype

    def update_definition_for_array(self, datatype, name):
        if datatype.initialized:
            return datatype
        if name:
            datatype.name = name
        subtype = datatype.subtype
        referred_size = datatype.dims * self.calc_data_type_size(subtype)
        referred_alignment = self.calc_data_type_alignment(subtype)
        if referred_size!=None and referred_alignment!=None:
            datatype.initialized = True
            datatype.size = referred_size
            datatype.alignment = referred_alignment
        else:
            set_trace()
            pass
        return datatype

    def update_definition_for_typedef(self, datatype, name):
        if datatype.initialized:
            return datatype
        if name:
            datatype.name = name
        if datatype.subtype.enum==data_types_enum_t.pointer:
            referred_size = self.target_word_size
            referred_alignment = self.target_word_size
        else:
            if datatype.subtype.name:
                subtype = self.definitions[datatype.subtype.name]
            else:
                subtype = datatype.subtype
            referred_size = self.calc_data_type_size(subtype)
            referred_alignment = self.calc_data_type_alignment(subtype)
        if referred_size!=None and referred_alignment!=None:
            datatype.initialized = True
            datatype.size = referred_size
            datatype.alignment = referred_alignment
        return datatype

    def update_definition_for_name(self, typename):
        datatype = self.definitions[typename]
        if datatype.enum==data_types_enum_t.enum:
            assert datatype.initialized
            return datatype
        elif datatype.enum==data_types_enum_t.basic:
            assert datatype.initialized
            return datatype
        elif datatype.enum==data_types_enum_t.pointer:
            datatype = self.update_definition_for_pointer(datatype, typename)
        elif datatype.enum==data_types_enum_t.struct:
            datatype = self.update_definition_for_struct(datatype, typename)
        elif datatype.enum==data_types_enum_t.typedef:
            datatype = self.update_definition_for_typedef(datatype, typename)
        else:
            set_trace(); pass
        self.definitions[typename] = datatype
        return datatype

    def update_definitions_recurse(self):
        items = self.definitions.items()
        for datatype_name, datatype in items:
            if datatype.size==None or datatype.alignment==None:
                self.update_definition_for_name(datatype_name)

    def decode_as_array(self, datatype):
        value = list()
        subtype = datatype.subtype
        is_basic_type = subtype.enum==data_types_enum_t.basic
        if is_basic_type:
            if subtype.name=='char':
                value = self.mmap_reader.get_bytes(datatype.size)
                return value[0:value.find(0)].decode()
            else:
                if subtype.is_signed:
                    value = self.mmap_reader.get_signed_int(subtype.size, datatype.dims)
                else:
                    value = self.mmap_reader.get_unsigned_int(subtype.size, datatype.dims)
        else:
            for i in range(datatype.dims):
                item = self.decode_as_type(subtype)
                value.append(item)
        return value

    def decode_as_enum(self, datatype):
        value = self.mmap_reader.get_signed_int(datatype.size)[0]
        try:
            return datatype.enums[value]
        except:
            #print ("Some error for enum value")
            return value
    
    def decode_as_pointer(self, datatype):
        return self.mmap_reader.get_unsigned_int(datatype.size)[0]

    def decode_as_struct(self, datatype):
        value = OrderedDict()
        curr_offset = self.mmap_reader.get_current_offset()
        for member in datatype.members_list.values():
            self.mmap_reader.set_current_offset(curr_offset + member.byte_offset)
            subtype = member.subtype
            if subtype.enum==data_types_enum_t.typedef:
                subtype = subtype.subtype
            value[member.name] = self.decode_as_type(subtype)
        self.mmap_reader.set_current_offset(curr_offset + datatype.size)
        return value



    def decode_as_type(self, datatype):
        if datatype.enum==data_types_enum_t.typedef:
            datatype = datatype.subtype
        if datatype.enum==data_types_enum_t.basic:
            if datatype.is_signed:
                return self.mmap_reader.get_signed_int(datatype.size)[0]
            else:
                return self.mmap_reader.get_unsigned_int(datatype.size)[0]
        elif datatype.enum==data_types_enum_t.enum:
            return self.decode_as_enum(datatype)
        elif datatype.enum==data_types_enum_t.array:
            return self.decode_as_array(datatype)
        elif datatype.enum==data_types_enum_t.pointer:
            return self.decode_as_pointer(datatype)
        elif datatype.enum==data_types_enum_t.struct:
            return self.decode_as_struct(datatype)
        elif datatype.enum==data_types_enum_t.typedef:
            datatype = datatype.subtype
            if datatype.enum==data_types_enum_t.basic:
                if datatype.is_signed:
                    return self.mmap_reader.get_signed_int(datatype.size)[0]
                else:
                    return self.mmap_reader.get_unsigned_int(datatype.size)[0]
            elif datatype.enum==data_types_enum_t.enum:
                return self.decode_as_enum(datatype)
            elif datatype.enum==data_types_enum_t.array:
                return self.decode_as_array(datatype)
            elif datatype.enum==data_types_enum_t.pointer:
                return self.decode_as_pointer(datatype)
            elif datatype.enum==data_types_enum_t.struct:
                return self.decode_as_struct(datatype)
            elif datatype.enum==data_types_enum_t.typedef:
                return self.decode_as_type(datatype.subtype)
            else:
                set_trace()
                pass
        else:
            set_trace()
            pass
