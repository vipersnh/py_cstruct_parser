import json
import pickle
import py_mmap_reader
from pdb import set_trace
from struct import unpack
from random import randint
from sys import stderr
from py_cstruct_parser import py_cstruct_parser_t

reader = py_mmap_reader.py_mmap_reader_t("trace.bin")
#parser = py_cstruct_parser_t("./l1df_lsat_info.c", reader)
parser = pickle.load(open("./cdefs.pickle", "rb"))
parser.mmap_reader = reader
while True:
    res = parser.decode_as_type(parser.definitions['l1df_trace_preamble'])
    print(json.dumps(res))

