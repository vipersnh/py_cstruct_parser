import argparse
import pickle
import py_mmap_reader
from pdb import set_trace
from struct import unpack
from random import randint
from sys import stderr
from py_cstruct_parser import py_cstruct_parser_t


if __name__ =="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('c_def_file', nargs=1)
    parser.add_argument('db_fname', nargs=1)
    parse_res = parser.parse_args()
    c_file = parse_res.c_def_file[0]
    database_file_name = parse_res.db_fname[0]

    db = py_cstruct_parser_t("./l1df_lsat_info.c", None)
    f_database = open(database_file_name, "wb")
    pickle.dump(db, f_database)
    f_database.close()
